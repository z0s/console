# z0s/Console

### Installation
You can include it in your project using: `composer require z0s/console`

### Requirements
1. PSR-11 compatible container
2. PHP8.0 or higher

### bin/console example

```php
<?php

$autoloaderPath = __DIR__ . '/../vendor/autoload.php';
if(!file_exists($autoloaderPath)) {
    throw new RuntimeException('Error, composer is not setup correctly.. Please run composer install');
}

$autoloader = require $autoloaderPath;

# Container
$container = new \League\Container\Container();

# Autowiring
$container->delegate(new \League\Container\ReflectionContainer());

# Load the CLI
$cli = new \z0s\Console\Console($container, $autoloader);

# Define the class scope to load commands from
$cli->setCommandsNamespace('z0s\\Commands');

# Define the name
$cli->setConsoleName('z0s');

# Define the version
$cli->setVersion('0.0.1');

# Run the cli
$cli->run();
```

### Command example

```php
<?php

namespace z0s\Commands;

/**
 * @property string $stringInput
 */
class Command extends \z0s\Console\ConsoleCommand
{
    protected string $signature = 'command {--stringInput=Hello World : Some string to pass into the command }';
    protected string $description = 'This is an example command';

    public function handle(): void
    {
        $this->out($this->stringInput);
    }
}
```
